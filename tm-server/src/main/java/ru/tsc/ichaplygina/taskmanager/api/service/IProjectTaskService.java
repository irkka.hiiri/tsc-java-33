package ru.tsc.ichaplygina.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.model.Project;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.Comparator;
import java.util.List;

public interface IProjectTaskService {

    void clearProjects(@NotNull String userId);

    @Nullable Task addTaskToProject(@NotNull String userId, @NotNull String projectId, @NotNull String taskId);

    @NotNull List<Task> findAllTasksByProjectId(@NotNull String userId,
                                                @NotNull String projectId,
                                                @NotNull String sortBy);

    @Nullable Project removeProjectById(@NotNull String userId, @Nullable String projectId);

    @Nullable Project removeProjectByIndex(@NotNull String userId, int index);

    @Nullable Project removeProjectByName(@NotNull String userId, @NotNull String projectName);

    @Nullable Task removeTaskFromProject(@NotNull String userId, @NotNull String projectId, @NotNull String taskId);
}
