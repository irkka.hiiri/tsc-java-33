package ru.tsc.ichaplygina.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.IBusinessEntityRepository;
import ru.tsc.ichaplygina.taskmanager.api.IBusinessEntityService;
import ru.tsc.ichaplygina.taskmanager.api.service.IAuthService;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.NameEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IndexIncorrectException;
import ru.tsc.ichaplygina.taskmanager.exception.security.AccessDeniedNotAuthorizedException;
import ru.tsc.ichaplygina.taskmanager.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import static ru.tsc.ichaplygina.taskmanager.util.ComparatorUtil.getComparator;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isInvalidListIndex;

public abstract class AbstractBusinessEntityService<E extends AbstractBusinessEntity> extends AbstractService<E> implements IBusinessEntityService<E> {

    @NotNull
    protected final IAuthService authService;

    @NotNull
    protected final IBusinessEntityRepository<E> repository;

    public AbstractBusinessEntityService(@NotNull final IBusinessEntityRepository<E> repository, @NotNull final IAuthService authService) {
        super(repository);
        this.repository = repository;
        this.authService = authService;
    }

    @Override
    public void add(@NotNull final String userId, @NotNull final String entityName, @Nullable final String entityDescription) {
        if (isEmptyString(entityName)) throw new NameEmptyException();
        repository.add(userId, entityName, entityDescription);
    }

    @Override
    public void clear(final String userId) {
        if (authService.isPrivilegedUser(userId)) repository.clear();
        else repository.clearForUser(userId);
    }

    @Nullable
    @Override
    public E completeById(@NotNull final String userId, @NotNull final String entityId) {
        return updateStatusById(userId, entityId, Status.COMPLETED);
    }

    @Nullable
    @Override
    public E completeByIndex(@NotNull final String userId, final int entityIndex) {
        return updateStatusByIndex(userId, entityIndex, Status.COMPLETED);
    }

    @Nullable
    @Override
    public E completeByName(@NotNull final String userId, @NotNull final String entityName) {
        return updateStatusByName(userId, entityName, Status.COMPLETED);
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId) {
        if (authService.isPrivilegedUser(userId)) return repository.findAll();
        return repository.findAllForUser(userId);
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId, @Nullable final String sortBy) {
        final Comparator<E> comparator = getComparator(sortBy);
        if (authService.isPrivilegedUser(userId)) return repository.findAll(comparator);
        return repository.findAllForUser(userId, comparator);
    }

    @Nullable
    @Override
    public E findById(@NotNull final String userId, @Nullable final String entityId) {
        if (isEmptyString(entityId)) throw new IdEmptyException();
        if (authService.isPrivilegedUser(userId)) return repository.findById(entityId);
        return repository.findByIdForUser(userId, entityId);
    }

    @Nullable
    @Override
    public E findByIndex(@NotNull final String userId, final int entityIndex) {
        if (isInvalidListIndex(entityIndex, getSize())) throw new IndexIncorrectException(entityIndex + 1);
        if (authService.isPrivilegedUser(userId)) return repository.findByIndex(entityIndex);
        return repository.findByIndexForUser(userId, entityIndex);
    }

    @Nullable
    @Override
    public E findByName(@NotNull final String userId, @NotNull final String entityName) {
        if (isEmptyString(entityName)) throw new NameEmptyException();
        if (authService.isPrivilegedUser(userId)) return repository.findByName(entityName);
        return repository.findByNameForUser(userId, entityName);
    }

    @Nullable
    @Override
    public String getId(@NotNull final String userId, final int entityIndex) {
        if (authService.isPrivilegedUser(userId)) return repository.getId(entityIndex);
        return repository.getIdForUser(userId, entityIndex);
    }

    @Nullable
    @Override
    public String getId(@NotNull final String userId, @NotNull final String entityName) {
        if (authService.isPrivilegedUser(userId)) return repository.getId(entityName);
        return repository.getIdForUser(userId, entityName);
    }

    @Override
    public int getSize(@NotNull final String userId) {
        if (authService.isPrivilegedUser(userId)) return repository.getSize();
        else return repository.getSizeForUser(userId);
    }

    @Override
    public boolean isEmpty(@NotNull final String userId) {
        if (authService.isPrivilegedUser(userId)) return repository.isEmpty();
        return repository.isEmptyForUser(userId);
    }

    @Override
    public boolean isNotFoundById(@NotNull final String userId, @NotNull final String entityId) {
        if (authService.isPrivilegedUser(userId)) return repository.isNotFoundById(entityId);
        return repository.isNotFoundByIdForUser(userId, entityId);
    }

    @Nullable
    @Override
    public E removeById(@NotNull final String userId, @NotNull final String entityId) {
        if (isEmptyString(entityId)) throw new IdEmptyException();
        if (authService.isPrivilegedUser(userId)) return repository.removeById(entityId);
        return repository.removeByIdForUser(userId, entityId);
    }

    @Nullable
    @Override
    public E removeByIndex(@NotNull final String userId, final int entityIndex) {
        if (isInvalidListIndex(entityIndex, getSize())) throw new IndexIncorrectException(entityIndex + 1);
        if (authService.isPrivilegedUser(userId)) return repository.removeByIndex(entityIndex);
        return repository.removeByIndexForUser(userId, entityIndex);
    }

    @Nullable
    @Override
    public E removeByName(@NotNull final String userId, @NotNull final String entityName) {
        if (isEmptyString(entityName)) throw new NameEmptyException();
        if (authService.isPrivilegedUser(userId)) return repository.removeByName(entityName);
        return repository.removeByNameForUser(userId, entityName);
    }

    @Nullable
    @Override
    public E startById(@NotNull final String userId, @NotNull final String entityId) {
        return updateStatusById(userId, entityId, Status.IN_PROGRESS);
    }

    @Nullable
    @Override
    public E startByIndex(@NotNull final String userId, final int entityIndex) {
        return updateStatusByIndex(userId, entityIndex, Status.IN_PROGRESS);
    }

     @Nullable
    @Override
    public E startByName(@NotNull final String userId, @NotNull final String entityName) {
        return updateStatusByName(userId, entityName, Status.IN_PROGRESS);
    }

    @Nullable
    @Override
    public E updateById(@NotNull final String userId,
                        @NotNull final String entityId,
                        @NotNull final String entityName,
                        @Nullable final String entityDescription) {
        if (isEmptyString(entityId)) throw new IdEmptyException();
        if (isEmptyString(entityName)) throw new NameEmptyException();
        if (authService.isPrivilegedUser(userId)) return repository.update(entityId, entityName, entityDescription);
        return repository.updateForUser(userId, entityId, entityName, entityDescription);
    }

    @Nullable
    @Override
    public E updateByIndex(@NotNull final String userId,
                           final int entityIndex,
                           @NotNull final String entityName,
                           @Nullable final String entityDescription) {
        if (isInvalidListIndex(entityIndex, getSize(userId))) throw new IndexIncorrectException(entityIndex + 1);
        @Nullable final String entityId = Optional.ofNullable(getId(userId, entityIndex)).orElse(null);
        if (entityId == null) return null;
        return updateById(userId, entityId, entityName, entityDescription);
    }

    @Nullable
    @Override
    public E updateStatusById(@NotNull final String userId,
                              @NotNull final String entityId,
                              @NotNull final Status status) {
        if (isEmptyString(entityId)) throw new IdEmptyException();
        if (authService.isPrivilegedUser(userId)) return repository.updateStatusById(entityId, status);
        return repository.updateStatusByIdForUser(userId, entityId, status);
    }

    @Nullable
    @Override
    public E updateStatusByIndex(@NotNull final String userId,
                                 final int projectIndex,
                                 @NotNull final Status status) {
        if (isInvalidListIndex(projectIndex, getSize(userId))) throw new IndexIncorrectException(projectIndex + 1);
        if (authService.isPrivilegedUser(userId)) return repository.updateStatusByIndex(projectIndex, status);
        return repository.updateStatusByIndexForUser(userId, projectIndex, status);
    }

    @Nullable
    @Override
    public E updateStatusByName(@NotNull final String userId,
                                @NotNull final String projectName,
                                @NotNull final Status status) {
        if (isEmptyString(projectName)) throw new NameEmptyException();
        if (authService.isPrivilegedUser(userId)) return repository.updateStatusByName(projectName, status);
        return repository.updateStatusByNameForUser(userId, projectName, status);
    }

}
