package ru.tsc.ichaplygina.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.service.IProjectService;
import ru.tsc.ichaplygina.taskmanager.api.service.IProjectTaskService;
import ru.tsc.ichaplygina.taskmanager.api.service.ISessionService;
import ru.tsc.ichaplygina.taskmanager.exception.entity.ProjectNotFoundException;
import ru.tsc.ichaplygina.taskmanager.model.Project;
import ru.tsc.ichaplygina.taskmanager.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;
import java.util.Optional;

@WebService(name = "ProjectEndpoint")
public final class ProjectEndpoint {

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final IProjectTaskService projectTaskService;

    @NotNull
    private final ISessionService sessionService;

    public ProjectEndpoint(@NotNull final IProjectService projectService,
                           @NotNull final IProjectTaskService projectTaskService,
                           @NotNull final ISessionService sessionService) {
        this.projectService = projectService;
        this.projectTaskService = projectTaskService;
        this.sessionService = sessionService;
    }

    @WebMethod
    public void clearProjects(@WebParam(name = "session") @Nullable final Session session) {
        sessionService.validateSession(session);
        projectTaskService.clearProjects(session.getUserId());
    }

    @WebMethod
    public void completeProjectById(@WebParam(name = "session") @Nullable final Session session,
                                    @WebParam(name = "projectId") @NotNull final String projectId) {
        sessionService.validateSession(session);
        Optional.ofNullable(projectService.completeById(session.getUserId(), projectId))
                .orElseThrow(ProjectNotFoundException::new);
    }

    @WebMethod
    public void completeProjectByIndex(@WebParam(name = "session") @Nullable final Session session,
                                       @WebParam(name = "projectIndex") final int projectIndex) {
        sessionService.validateSession(session);
        Optional.ofNullable(projectService.completeByIndex(session.getUserId(), projectIndex))
                .orElseThrow(ProjectNotFoundException::new);
    }

    @WebMethod
    public void completeProjectByName(@WebParam(name = "session") @Nullable final Session session,
                                      @WebParam(name = "projectName") @NotNull final String projectName) {
        sessionService.validateSession(session);
        Optional.ofNullable(projectService.completeByName(session.getUserId(), projectName))
                .orElseThrow(ProjectNotFoundException::new);
    }

    @WebMethod
    public void createProject(@WebParam(name = "session") @Nullable final Session session,
                              @WebParam(name = "projectName") @NotNull final String projectName,
                              @WebParam(name = "projectDescription") @Nullable final String projectDescription) {
        sessionService.validateSession(session);
        projectService.add(session.getUserId(), projectName, projectDescription);
    }

    @WebMethod
    public List<Project> getProjectList(@WebParam(name = "session") @Nullable final Session session,
                                        @WebParam(name = "sortBy") @Nullable final String sortBy) {
        sessionService.validateSession(session);
        return projectService.findAll(session.getUserId(), sortBy);
    }

    @WebMethod
    public Project findProjectById(@WebParam(name = "session") @Nullable final Session session,
                                   @WebParam(name = "projectId") @NotNull final String projectId) {
        sessionService.validateSession(session);
        return projectService.findById(session.getUserId(), projectId);
    }

    @WebMethod
    public Project findProjectByIndex(@WebParam(name = "session") @Nullable final Session session,
                                      @WebParam(name = "projectIndex") final int projectIndex) {
        sessionService.validateSession(session);
        return projectService.findByIndex(session.getUserId(), projectIndex);
    }

    @WebMethod
    public Project findProjectByName(@WebParam(name = "session") @Nullable final Session session,
                                     @WebParam(name = "projectName") @NotNull final String projectName) {
        sessionService.validateSession(session);
        return projectService.findByName(session.getUserId(), projectName);
    }

    @WebMethod
    public void removeProjectById(@WebParam(name = "session") @Nullable final Session session,
                                  @WebParam(name = "projectId") @NotNull final String projectId) {
        sessionService.validateSession(session);
        Optional.ofNullable(projectService.removeById(session.getUserId(), projectId))
                .orElseThrow(ProjectNotFoundException::new);
    }

    @WebMethod
    public void removeProjectByIndex(@WebParam(name = "session") @Nullable final Session session,
                                     @WebParam(name = "projectIndex") final int projectIndex) {
        sessionService.validateSession(session);
        Optional.ofNullable(projectService.removeByIndex(session.getUserId(), projectIndex))
                .orElseThrow(ProjectNotFoundException::new);
    }

    @WebMethod
    public void removeProjectByName(@WebParam(name = "session") @Nullable final Session session,
                                    @WebParam(name = "projectName") @NotNull final String projectName) {
        sessionService.validateSession(session);
        Optional.ofNullable(projectService.removeByName(session.getUserId(), projectName))
                .orElseThrow(ProjectNotFoundException::new);
    }

    @WebMethod
    public void startProjectById(@WebParam(name = "session") @Nullable final Session session,
                                 @WebParam(name = "projectId") @NotNull final String projectId) {
        sessionService.validateSession(session);
        Optional.ofNullable(projectService.startById(session.getUserId(), projectId))
                .orElseThrow(ProjectNotFoundException::new);
    }

    @WebMethod
    public void startProjectByIndex(@WebParam(name = "session") @Nullable final Session session,
                                    @WebParam(name = "projectIndex") final int projectIndex) {
        sessionService.validateSession(session);
        Optional.ofNullable(projectService.startByIndex(session.getUserId(), projectIndex))
                .orElseThrow(ProjectNotFoundException::new);
    }

    @WebMethod
    public void startProjectByName(@WebParam(name = "session") @Nullable final Session session,
                                   @WebParam(name = "projectName") @NotNull final String projectName) {
        sessionService.validateSession(session);
        Optional.ofNullable(projectService.startByName(session.getUserId(), projectName))
                .orElseThrow(ProjectNotFoundException::new);
    }

    @WebMethod
    public void updateProjectById(@WebParam(name = "session") @Nullable final Session session,
                                  @WebParam(name = "projectId") @NotNull final String projectId,
                                  @WebParam(name = "projectName") @NotNull final String projectName,
                                  @WebParam(name = "projectDescription") @Nullable final String projectDescription) {
        sessionService.validateSession(session);
        Optional.ofNullable(projectService.updateById(session.getUserId(), projectId, projectName, projectDescription))
                .orElseThrow(ProjectNotFoundException::new);
    }

    @WebMethod
    public void updateProjectByIndex(@WebParam(name = "session") @Nullable final Session session,
                                     @WebParam(name = "projectIndex") final int projectIndex,
                                     @WebParam(name = "projectName") @NotNull final String projectName,
                                     @WebParam(name = "projectDescription") @Nullable final String projectDescription) {
        sessionService.validateSession(session);
        Optional.ofNullable(projectService.updateByIndex(session.getUserId(), projectIndex, projectName, projectDescription))
                .orElseThrow(ProjectNotFoundException::new);
    }

}
